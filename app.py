from flask import Flask, render_template, request, redirect, url_for, flash
from flask_mysqldb import MySQL

app = Flask(__name__)
app.secret_key = 'many random bytes'

app.config['MYSQL_HOST'] = 'localhost'
app.config['MYSQL_USER'] = 'root'
app.config['MYSQL_PASSWORD'] = '12345678'
app.config['MYSQL_DB'] = 'todolist'

mysql = MySQL(app)

@app.route('/')
def Index():
    cur = mysql.connection.cursor()
    cur.execute("SELECT * FROM tasks")
    data = cur.fetchall()
    cur.close()
    return render_template('index.html', tasks=data )

@app.route('/insert', methods = ['POST'])
def insert():
    if request.method == "POST":
        taskscontent = request.form['taskscontent']
        cur = mysql.connection.cursor()
        cur.execute(""" INSERT INTO tasks (taskscontent) VALUES (%s) """, (taskscontent,))
        mysql.connection.commit()
        flash("Data Inserted Successfully")
        return redirect(url_for('Index'))

@app.route('/delete/<string:idtasks_data>', methods = ['GET'])
def delete(idtasks_data):
    flash("Record Has Been Deleted Successfully")
    cur = mysql.connection.cursor()
    cur.execute("DELETE FROM tasks WHERE idtasks=%s", (idtasks_data,))
    mysql.connection.commit()
    return redirect(url_for('Index'))

@app.route('/update',methods=['POST','GET'])
def update():
    if request.method == 'POST':
        idtasks_data = request.form['idtasks']
        taskscontent = request.form['taskscontent']
        cur = mysql.connection.cursor()
        cur.execute(""" UPDATE tasks SET taskscontent=%s WHERE idtasks=%s """, (taskscontent, idtasks_data))
        flash("Data Updated Successfully")
        mysql.connection.commit()
        return redirect(url_for('Index'))

if __name__ == "__main__":
    app.run(debug=True)